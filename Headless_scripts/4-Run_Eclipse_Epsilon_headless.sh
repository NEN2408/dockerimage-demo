#!/bin/bash

# Variables for commands
LAUNCHERJAR=./epsilon/plugins/org.eclipse.equinox.launcher_*.jar
WORKSPACE=./epsilon_playground-example/

# Parameters coming from the command line
ANT_FILE="$1"
ANT_TARGET="$2"

# Virtual X Frame Buffer Run Java (fake having an X Desktop)
# 	java run the Eclipse Launcher with these parameters
#		- workspace path to our the epsilon playground example
#		- execute the antRunner application
#		- antRunner needs these buildfile instructions
xvfb-run java -ea -jar ${LAUNCHERJAR} \
	-data ${WORKSPACE} \
	-application org.eclipse.ant.core.antRunner \
	-buildfile "${ANT_FILE}" "${ANT_TARGET}"

# If there was an error, read the Eclipse logs to the console and exit
if [[ $? -gt 0 ]]
then
	cat ${WORKSPACE}/.metadata/.log
	exit 1
fi

# Otherwise, exit 0, meaning success!
exit 0