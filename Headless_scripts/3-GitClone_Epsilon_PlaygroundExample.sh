#!/bin/bash

# Epsilon playground-example project from a GitLab repository
git clone https://gitlab.com/NEN2408/epsilon_playground-example.git

# Show folder changes
pwd
ls -1sh
echo "./epsilon_playground-example"
ls -1sh  ./epsilon_playground-example 
