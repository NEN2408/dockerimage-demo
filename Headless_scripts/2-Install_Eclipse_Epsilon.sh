#!/bin/bash

# Change directory to the eclipse-installer
cd eclipse-installer

# Variable with all the repositories the installer needs
P2_REPOSITORIES=\
https://mirror.aarnet.edu.au/pub/eclipse/releases/latest,\
https://mirror.aarnet.edu.au/pub/eclipse/eclipse/updates/latest,\
https://mirror.aarnet.edu.au/pub/eclipse/epsilon/interim,\
https://mirror.aarnet.edu.au/pub/eclipse/emfatic/update,\
https://mirror.aarnet.edu.au/pub/eclipse/modeling/gmp/gmf-tooling/updates/releases,\
https://mirror.aarnet.edu.au/pub/eclipse/birt/update-site/latest

# Variable with all the installable units to be installed from the repositories
INSTALLABLE_UNITS=\
org.eclipse.sdk.ide,\
org.eclipse.epsilon.core.dt.feature.feature.group,\
org.eclipse.epsilon.core.feature.feature.group,\
org.eclipse.epsilon.emf.dt.feature.feature.group,\
org.eclipse.epsilon.emf.feature.feature.group,\
org.eclipse.epsilon.eunit.dt.emf.feature.feature.group,\
org.eclipse.epsilon.evl.emf.validation.feature.feature.group,\
org.eclipse.epsilon.hutn.dt.feature.feature.group,\
org.eclipse.emf,\
org.eclipse.emf.cdo.sdk.feature.group,\
org.eclipse.emf.emfatic.core,\
org.eclipse.wst.wsdl,\
org.eclipse.xsd.sdk.feature.group

# Run a really BIG installation command! Everything installs to ../epsilon
./eclipse-inst \
-nosplash \
-consoleLog \
-application org.eclipse.equinox.p2.director \
-repository ${P2_REPOSITORIES} \
-installIU ${INSTALLABLE_UNITS} \
-profile SDKProfile \
-profileProperties org.eclipse.update.install.features=true \
-destination ../epsilon

# Show folder changes
cd..
pwd
ls -1sh