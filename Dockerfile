# The base image that this new image will be built upon (eclipse-temurin Version 17)
FROM eclipse-temurin:17

### STAGE 1 ###
# Install Packages
###############

# Disable interactivity for install of git
ARG DEBIAN_FRONTEND=noninteractive
RUN echo "Running on arch '${TARGETARCH}'"

# The base image is using apt package management, so we apt for applications needed
RUN apt-get update
RUN apt-get upgrade --yes
RUN apt-get install --yes --no-install-recommends \
xvfb \
dbus-x11 \
firefox \
libxtst6 \
file \
wget \
git \
nano

# Clean up apt to keep the new image small
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/*

# Set the directory inside the container where following work (commands) occur
WORKDIR /


### STAGE 2 ###
# Install eclipse director
###############

# Push in the download script and make it executable
COPY /Headless_scripts/1-Download_Eclipse_Installer.sh /
RUN chmod 755 /1-Download_Eclipse_Installer.sh

# Run the download script and unpack the Eclipse installer
RUN ./1-Download_Eclipse_Installer.sh


### STAGE 3 ###
# Install "base" eclipse
###############

# Push in the install script and make it executable
COPY /Headless_scripts/2-Install_Eclipse_Epsilon.sh /
RUN chmod 755 /2-Install_Eclipse_Epsilon.sh

# Run the Eclipse Installer headless
RUN ./2-Install_Eclipse_Epsilon.sh


### STAGE 4 ###
# Prime the containers entrypoint (when it starts we want it to run a script)
###############

# Push in the run script and make it executable
COPY /Headless_scripts/4-Run_Eclipse_Epsilon_headless.sh /
RUN chmod 755 /4-Run_Eclipse_Epsilon_headless.sh

# Setting the entry point to be the script
ENTRYPOINT [ "/4-Run_Eclipse_Epsilon_headless.sh" ]

# advertise volume mount where the Epsilon project should be attached
VOLUME [ "/epsilon_playground-example" ]